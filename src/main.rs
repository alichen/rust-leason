mod list;
mod city;
mod slice;
mod string;
mod structs;
mod enums;
mod from;
mod closure;
mod matchs;
mod traits;
mod panics;
mod boxs;
mod threads;
mod process;
mod file;
mod test;
mod pointers;
mod nodes;
mod macros;

fn main() {
  // println!("now {:#?}", Todo(32))
  // crate::City
  // city
  let val1 = city::City{
    name: "beijing",
    lat: 33.3,
    lng: 22.2
  };
  let val = list::List(vec![1, 2, 3]);
  println!("{}", val);
  println!("{}", val1);
  println!("code is {}", enums::CODE);

  slice::todo();
  string::todo();
  structs::todo();
  enums::todo();
  closure::base::todo();
  from::todo();
  matchs::todo();
  traits::todo();
  panics::todo();
  boxs::todo();
  threads::todo();
  threads::foo();
  threads::open();
  threads::read();
  process::todo();
  file::todo();
  pointers::todo();
  nodes::todo();
  macros::todo();
}
