use std::fmt;

pub struct List(pub Vec<i32>);

// pub code:i32 = 9527;

impl fmt::Display for List{
    // fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      let vec = &self.0;
      write!(f, "[")?;
      for (count, v) in vec.iter().enumerate() {
        if count != 0 {
          write!(f, ", ").expect("seperator should write");
        }
        write!(f,"{}", v).expect("var should write");
      }
      write!(f,"]")
    }
}