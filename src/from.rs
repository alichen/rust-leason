use std::convert::TryFrom;
use std::convert::TryInto;

#[derive(Debug, PartialEq)]
struct Number{
  val: i32
}

impl From<i32> for Number{
  fn from(i: i32) ->Self {
    Number{ val: i}
  }
}

#[derive(Debug, PartialEq)]
struct EvenNumber(i32);

impl TryFrom<i32> for EvenNumber{
  type Error = ();
  fn try_from(i: i32) -> Result<Self, Self::Error> {
    if i % 2 == 0 {
      Ok(EvenNumber(i))
    } else {
      Err(())
    }
  }
}

pub fn todo() {
  let a = Number::from(5);
  println!("a is {:#?}", a);
  let b: Number = 6.into();
  println!("b is {:#?}", b);

  assert_eq!(EvenNumber::try_from(4), Ok(EvenNumber(4)));
  let c: Result<EvenNumber, ()> = 4i32.try_into();
  assert_eq!(c, Ok(EvenNumber(4)));

  let d: i32 = "18".parse::<i32>().unwrap();
  println!("d is {:#?}", d);
}