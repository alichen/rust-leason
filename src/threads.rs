use std::thread;

const CNT:i32 = 10;
pub fn todo() {
    let mut children = vec![];
    for i in 0..CNT{
        children.push(thread::spawn(move || {
            println!("thread number {}", i)
        }))
    }
    for child in children{
        let _ = child.join();
    }
}

use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;

pub fn foo() {
    let (tx, rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();
    for id in 0..CNT{
        let t_tx = tx.clone();
        thread::spawn(move ||{
            t_tx.send(id).unwrap();
            println!("thread {} finished", id);
        });
    }
    let mut ids = Vec::with_capacity(CNT as usize);
    for _ in 0..CNT{
        ids.push(rx.recv());
    }
    println!("{:?}", ids);
}

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io;

pub fn open() {
    let p = Path::new("Cargo.toml");
    let display = p.display();
    let mut file = match File::open(&p) {
        Ok(file) => file,
        Err(err) => panic!("could not open {}: {}", display, err),
    };
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Ok(_) => print!("{} contains", s),
        Err(err) => panic!("could not read {}: {}", display, err),
    }
}
fn read_lines<P>(filename: P)  -> io::Result<io::Lines<io::BufReader<File>>> where P: AsRef<Path>{
    let f = File::open(filename)?;
    Ok(io::BufReader::new(f).lines())
}

pub fn read() {
    if let Ok(lines) = read_lines("./Cargo.toml") {
        for line in lines {
            if let Ok(ip) = line {
                println!("{}", ip);
            }
        }
    }
}
