use std::process::{Command};

pub fn todo() {
    let output = Command::new("rustc").arg("--version").output()
        .unwrap_or_else(|e|{
            panic!("failed to exec {}", e)
        });
    let s;
    if output.status.success() {
        s = String::from_utf8_lossy(&output.stdout);
    } else {
        s = String::from_utf8_lossy(&output.stderr);
    }
    println!("result is {}", s);

    //let pangram: &'static str = "hello world";

    //let process = match Command::new("wc")
    //    .stdin(Stdio::piped())
    //    .stdout(Stdio::piped())
    //    .spawn() {
    //        Ok(process) => process,
    //        Err(why) => panic!("failed to spawn {}", why),
    //};
    //match process.stdin.unwrap().write_all(pangram.as_bytes()descriptiondescription) {
    //    Ok(_) => println!("write success"),
    //    Err(why) => panic!("failed to write, {}", why.description()),
    //}
    //let mut ret = String::new();
    //match process.stdout.unwrap().read_to_string(&mut ret) {
    //    Ok(_) => println!("writed {}", ret),
    //    Err(why) => panic!("failed to read {}", why.description()),
    //}
}
