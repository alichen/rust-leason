use std::fmt;

pub struct City {
  pub name: &'static str,
  pub lat: f32,
  pub lng: f32,
}

impl fmt::Display for City  {
  fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
    let lat_c = if self.lat >= 0.0 {"N"} else {"S"};
    let lng_c = if self.lng >= 0.0 {"E"} else {"W"};
    write!(f, "{}: {:3}-{} {:3}-{}", self.name, self.lat.abs(), lat_c, self.lng.abs(), lng_c)
  }
}
