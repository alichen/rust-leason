pub fn todo() {
  struct Point {
    x: i32,
    y: i32,
  }

  let point = Point{x: 1, y:2};

  let Point { x: my_x, y:my_y} = point;
  println!("x is {} y is{}", my_x, my_y);
}