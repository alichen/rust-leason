use std::mem;

fn analyze_slice(slice: &[i32]) {
  println!("the slice has {} elemenets", slice.len());
  // println!("ths slice is {:#?}", slice);
}

pub fn todo() {
  let a = [1, 2, 3, 4, 5];

  let b: [i32; 100] = [5; 100];
  println!("a size is {}", mem::size_of_val(&a));

  analyze_slice(&a);
  analyze_slice(&b);
  analyze_slice(&a[1..(a.len() - 2)]);
}
