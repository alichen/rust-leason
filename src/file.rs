use std::fs;
use std::fs::{File, OpenOptions};
use std::io;
use std::io::prelude::*;
use std::path::Path;

#[allow(dead_code)]
fn cat(path: &Path) -> io::Result<String> {
    let mut f = File::open(path)?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}

#[allow(dead_code)]
fn echo(s: &'static str, path: &Path) -> io::Result<()> {
    let mut f = File::create(path)?;
    f.write_all(s.as_bytes())
}

#[allow(dead_code)]
fn touch(path: &Path) -> io::Result<()> {
    match OpenOptions::new().read(true).write(true).open(path) {
        Ok(_) => Ok(()),
        Err(e) =>Err(e),
    }
}

pub fn todo() {
    match fs::create_dir("a") {
        Ok(_) => {},
        Err(why) => println!("failed to create dir {}", why),
    }
    echo("hello", &Path::new("a/b.txt")).unwrap_or_else(|why|{
        println!("failed to echo file {}", why);
    });
}
