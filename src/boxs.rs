#[allow(dead_code)]
struct Point {
    x: i32,
    y: i32
}

use std::mem;

pub fn todo() {
    //let p = Point{x: 1, y: 2};
    //let boxed_p: Box<Point> = Box::new(p);
    println!("boxed_p size is {}", mem::size_of_val(&3i32));
}
