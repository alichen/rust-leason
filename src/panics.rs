#[allow(dead_code)]

#[derive(Debug)] enum Food {
    Carrot, Apple, Potato
}

#[derive(Debug)] struct Peeled(Food);
#[derive(Debug)] struct Choped(Food);
#[derive(Debug)] struct Cooked(Food);

fn peel(food: Option<Food>) -> Option<Peeled> {
    food.map(|f|Peeled(f))
}

fn chop(food: Option<Peeled>) -> Option<Choped> {
    food.map(|Peeled(f)|Choped(f))
}

fn eat(food: Option<Choped>) {
    match food {
        Some(food) => println!("its good {:?}", food),
        None => println!("i cant eat")
    }
}
use std::num::ParseIntError;

#[allow(dead_code)]
fn multi(first: &str, second:&str) -> Result<i32, ParseIntError> {
    match first.parse::<i32>() {
        Ok(first_number) => {
            match second.parse::<i32>() {
                Ok(second_number) => {
                    Ok(first_number + second_number)
                },
                Err(e) => Err(e),
            }
        },
        Err(e) => Err(e),
    }
}

fn multi_v2(first: &str, second: &str) -> Result<i32, ParseIntError> {
    first.parse::<i32>().and_then(|first_number|{
        second.parse::<i32>().map(|second_number| first_number + second_number)
    })
}

fn double_first(vec: Vec<&str>) -> Result<Option<i32>, ParseIntError> {
    let opt = vec.first().map(|first| {
        first.parse::<i32>().map(|n|3*n)
    });
    opt.map_or(Ok(None), |r|r.map(Some))
}

pub fn todo() {
    let apple = Some(Food::Apple);
    eat(chop(peel(apple)));
    if let Ok(ret) = multi_v2("10", "20") {
        println!("ret is {}", ret);
    } else {
        println!("wrong");
    }
    let vec = vec!["10", "20"];
    println!("double is {:?}",double_first(vec));
    //eat(peel(apple).and_then(chop))

}
