struct Message<'a> {
  id :i32,
  name: &'a str,
}

pub fn todo() {
  let refs = &4;
  match refs {
    &val => println!("got val {}", val)
  }
  match refs {
    ref val => println!("got ref val {}", val)
  }
  let mut mut_value = 6;
  match mut_value {
    ref mut m => {
      *m += 2;
      println!("got mut val {}", m);
    }
  }
  let msg = Message { id: 5, name: "todo"};
  match msg {
    Message { id: id@3..=4, ..} => {
      println!("id is {}", id);
    },
    Message { id, name} => {
      println!("id is not {}, {}", id, name);
    }
  }
}
