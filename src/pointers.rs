use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug)]
enum List {
  Cons(Rc<RefCell<i32>>, Rc<List>),
  Nil,
}

use List::{Cons, Nil};

pub fn todo() {
  let val = Rc::new(RefCell::new(5));
  let a = Rc::new(Cons(Rc::clone(&val), Rc::new(Nil)));
  let b = Cons(Rc::new(RefCell::new(6)), Rc::clone(&a));
  let c = Cons(Rc::new(RefCell::new(7)), Rc::clone(&a));

  *val.borrow_mut() += 10;
  // let a = Rc::new(Cons(1, Rc::new(Cons(2, Rc::new(Cons(3, Rc::new(Nil)))))));
  // let b = Cons(4, Rc::clone(&a));
  // let c = Cons(5, Rc::clone(&a));
  println!("list is {:?}, {:?}", b, c);
}