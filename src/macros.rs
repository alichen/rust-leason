macro_rules!  hi{
  ($fn_name: ident) => {
    println!("you called {}", stringify!($fn_name));
  };
}

macro_rules! overrides {
  ($left: expr, and $right: expr) => {
    println!("ret is {}", $left && $right);
  };
  ($left: expr, or $right: expr) => {
    println!("ret is {}", $left || $right);
  };
}

macro_rules! minimal {
  ($a: expr) => {
    $a
  };
  ($a:expr, $($b:expr),+) => {
    std::cmp::min($a, minimal!($($b),+))
  }
}

pub fn todo() {
  hi!(foo);
  overrides!(1 + 1 == 2,and 2 + 2 == 4);
  println!("minimal is {}", minimal!(1));
  println!("minimal is {}", minimal!(2,3,4));
}