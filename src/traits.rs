trait Contains {
    type A;
    type B;

    fn contains(&self, _:Self::A, _:Self::B)->bool;
    fn first(&self)->i32;
    fn last(&self)->i32;
}

struct Container(i32, i32);

impl Contains for Container{
    type A = i32;
    type B = i32;
    fn contains(&self, a:i32, b:i32) -> bool {
        self.0 == a && self.1 == b
    }
    fn first(&self) -> i32 { self.0 }
    fn last(&self) -> i32 { self.1 }
}

#[allow(dead_code)]
fn diff<C: Contains>(c: C) -> i32{
    c.last() - c.first()
}

pub fn todo() {
    let c = Container(2, 3);
    let a = 2i32;
    let b = 3i32;
    println!("c contains {}", c.contains(a, b));

}
