use List::{ Cons, Nil };

pub static CODE: &'static str = "123";

enum List{
  Cons(u32, Box<List>),
  Nil
}

impl List {
  fn new() -> List{
    Nil
  }
  fn prepend(self, el: u32) -> List {
    Cons(el, Box::new(self))
  }
  fn stringify(&self) -> String {
    match *self {
      Cons(head, ref tail) => {
        format!("{}, {}", head, tail.stringify())
      },
      Nil => {
        format!("Nil")
      }
    }
  }
}

pub fn todo() {
  let mut list = List::new();
  list = list.prepend(1);
  println!("list is {}", list.stringify());
}