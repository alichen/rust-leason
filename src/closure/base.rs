fn apply_once<F>(f: F) where F: FnOnce() {
    f();
}

fn apply_to<F>(f: F) where F: Fn(i32) ->i32 {
    f(3);
}

fn create_fn() -> impl Fn() {
    let text ="123".to_owned();
    move || println!("this is {}", text)
}

pub fn todo() {
    let mut cnt = 0;
    let mut add = || cnt += 2;
    add();
    println!("closure got val {}", cnt);
    let daily = || {
        println!("im daily");
    };
    apply_once(daily);
    let double = |x|x*2;
    apply_to(double);
    let prints = create_fn();
    prints();
}
